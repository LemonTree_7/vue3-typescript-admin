import "@babel/polyfill";
import { createApp } from "vue";
import store from "@/stores";
import I18n from "@/language/index";

import "@/assets/styles/normalize.css";
import "element-plus/dist/index.css";
import "@/assets/styles/index.scss";

import App from "./App.vue";
import router from "./router";

import * as ElementPlusIconsVue from "@element-plus/icons-vue";
// import ElementPlus from "element-plus";

import tools from "@/utils/tools";
import { getLocalStorage } from "@/utils/auth";

const app = createApp(App);
// app.use(ElementPlus);
// 导入elementUI图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}

tools.initTranslate("english");
tools.changeTranslate(getLocalStorage("to"));

translate.listener.renderTaskFinish = function (task) {
  console.log("执行完一次");
};

app.use(store).use(router).use(I18n);
app.config.errorHandler = (err, instance, info) => {
  // 处理错误，例如：报告给一个服务
  console.error("errorHandler", err, instance, info);
};
app.config.warnHandler = (err, instance, info) => {
  console.warn("warnHandler", err, instance, info);
};
app.mount("#app");
