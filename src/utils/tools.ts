// 全局通用工具
export default {
  // 空白显示
  blankDisplay(value: any) {
    return value || "xxx";
  },
  // 拆分数组每个数组多少个
  arrayChunk(array: any, size: any) {
    let data = [];
    for (let i = 0; i < array.length; i += size) {
      data.push((array || []).slice(i, i + size));
    }
    return data;
  },
  remove(arr: any, val: any) {
    const index = arr.indexOf(val);
    if (index > -1) {
      arr.splice(index, 1);
    }
  },
  routerPush(_this: any, params = {}) {
    let routeData = _this.$router.resolve(params);
    window.open(routeData.href, "_blank");
  },
  openUrl(path: any) {
    window.open(path, "_blank");
  },
  isMobile(userAgent: any, isNotWindow: any) {
    var userAgentInfo = userAgent || (navigator && navigator.userAgent) || "";

    var mobileAgents = [
      "Android",
      "iPhone",
      "SymbianOS",
      "Windows Phone",
      "iPad",
      "iPod",
    ];

    var mobile_flag = false;

    //根据userAgent判断是否是手机
    for (var v = 0; v < mobileAgents.length; v++) {
      if (userAgentInfo.indexOf(mobileAgents[v]) > 0) {
        mobile_flag = true;
        break;
      }
    }

    if (isNotWindow) {
      return mobile_flag;
    }

    var screen_width = window.screen.width;
    var screen_height = window.screen.height;

    //根据屏幕分辨率判断是否是手机
    if (screen_width > 325 && screen_height < 750) {
      mobile_flag = true;
    }

    return mobile_flag;
  },
  // 获取静态图片
  getAssetsFile(url: string) {
    return new URL(`../assets/images/${url}`, import.meta.url).href;
  },
  // 防抖
  debounce(func, wait, immediate) {
    let timer;
    return function () {
      let context = this,
        args = arguments;

      if (timer) clearTimeout(timer);
      if (immediate) {
        let callNow = !timer;
        timer = setTimeout(() => {
          timer = null;
        }, wait);
        if (callNow) func.apply(context, args);
      } else {
        timer = setTimeout(() => {
          func.apply(context, args);
        }, wait);
      }
    };
  },
  // 节流
  throttle(func, wait, type) {
    if (type === 1) {
      let previous = 0;
    } else if (type === 2) {
      let timeout;
    }
    return function () {
      let context = this;
      let args = arguments;
      if (type === 1) {
        let now = Date.now();

        if (now - previous > wait) {
          func.apply(context, args);
          previous = now;
        }
      } else if (type === 2) {
        if (!timeout) {
          timeout = setTimeout(() => {
            timeout = null;
            func.apply(context, args);
          }, wait);
        }
      }
    };
  },
  //客户端进行翻译
  // 具体翻译文档http://translate.zvo.cn/index.html
  initTranslate(language) {
    translate.ignore.class.push("test");
    translate.language.setDefaultTo(language);
    translate.language.setLocal(language);
    translate.selectLanguageTag.show = false;
    translate.listener.start();
  },
  changeTranslate(language) {
    translate.changeLanguage(language);
  },
  translate() {
    // console.log(translate, "translate");
    translate.execute();
  },
  replaceAll(value = "", template = "", replaceVal = "") {
    if (!value) return "";
    return value.replace(new RegExp(template, "gm"), replaceVal);
  },
};
