const date = {
  //格式化时间
  format: function (fmt, oldDate = new Date()) {
    let date = oldDate;
    if (!date || date.toString() == "Invalid Date") return "";
    // 是否为ios
    if (!!navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) {
      if (typeof date == "string" && date.indexOf("-") > -1)
        date = date.replace(/\-/g, "/");
      date = new Date(date);
    }
    const o = {
      "M+": new Date(date).getMonth() + 1, // 月份
      "d+": new Date(date).getDate(), // 日
      "h+": new Date(date).getHours(), // 小时
      "m+": new Date(date).getMinutes(), // 分
      "s+": new Date(date).getSeconds(), // 秒
      "q+": Math.floor((new Date(date).getMonth() + 3) / 3), // 季度
      S: new Date(date).getMilliseconds(), // 毫秒
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(
        RegExp.$1,
        (new Date(date).getFullYear() + "").substr(4 - RegExp.$1.length)
      );
    }
    for (const k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(
          RegExp.$1,
          RegExp.$1.length === 1
            ? o[k]
            : ("00" + o[k]).substr(("" + o[k]).length)
        );
      }
    }
    return fmt;
  },
  // 获取当前月多少天
  getMonthDays: (year, month) => new Date(year, month, 0).getDate(),
  // 计算多少天前
  getDaysAgo: (date, days) => {
    const newDate = date || new Date();
    const t = new Date(newDate).getTime() + days * 24000 * 3600; //获取n天的时间，time
    return new Date().setTime(t);
  },
  // 计算几个月前 （当前日期，月）
  getMonthsAgo: (date, months) => {
    const newDate = date || new Date();
    const t = new Date(newDate).setMonth(months);
    return t;
  },
  //  计算今天是星期几
  getWeekDay: (date) => {
    const newDate = date || new Date();
    return new Date(newDate).getDay();
  },
  getDiffDay: (date_1, days) => {
    let totalDays, diffDate, totalhous;
    let myDate_1 = Date.parse(date_1);
    var dd = new Date(date_1);
    dd.setDate(dd.getDate() + days); //获取p_count天后的日期
    var y = dd.getFullYear();
    var m = dd.getMonth() + 1; //获取当前月份的日期
    if (m < 10) {
      m = "0" + m;
    }
    var d = dd.getDate();
    if (d < 10) {
      d = "0" + d;
    }
    var h = dd.getHours();
    if (h < 10) {
      h = "0" + h;
    }
    var f = dd.getMinutes();
    if (f < 10) {
      f = "0" + f;
    }
    var s = dd.getSeconds();
    if (s < 10) {
      s = "0" + s;
    }
    let myDate_2 = Date.parse(
      y + "-" + m + "-" + d + " " + h + ":" + f + ":" + s
    );
    console.log(
      "myDate_2myDate_2",
      days,
      y + "-" + m + "-" + d + " " + h + ":" + f + ":" + s,
      myDate_2,
      myDate_1
    );
    let myDate_1xz = Date.parse(new Date());
    diffDate = myDate_2 - myDate_1xz;
    var days = Math.floor(diffDate / (24 * 3600 * 1000)); //天  24*60*60*1000
    var leave1 = diffDate % (24 * 3600 * 1000); //计算天数后剩余的毫秒数
    var hours = Math.floor(leave1 / (3600 * 1000));

    var leave2 = leave1 % (3600 * 1000); //计算小时数后剩余的毫秒数
    var minutes = Math.floor(leave2 / (60 * 1000));

    var leave3 = leave2 % (60 * 1000); //计算分钟数后剩余的毫秒数
    var seconds = Math.round(leave3 / 1000);
    console.log("22", days, hours, minutes, seconds);
    return days + " days " + hours + " hours " + minutes + " minutes ";
  },
};
// 防抖
function debounce(fn, delay) {
  let timer = null;
  return function () {
    clearTimeout(timer);
    timer = setTimeout(() => {
      fn.apply(this, arguments);
    }, delay);
  };
}

export { date, debounce };
