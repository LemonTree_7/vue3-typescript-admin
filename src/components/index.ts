/* 统一管理全局组件的地方 */
import type { App } from 'vue';

const component:any = {

};

const appComponent = {
	install(app: App) {
		for (const key in component) {
			/**
			 * @description 注册全局组件
			 * @param key 组件名  component[key] 组件对应的文件
			 */
			app.component(key, component[key]);
		}
	}
};

export default appComponent;
