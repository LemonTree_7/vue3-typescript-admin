import { Get } from "../app";

class UserApi {
  async login(data) {
    return Get(`/login`, data);
  }
}

export default new UserApi();
